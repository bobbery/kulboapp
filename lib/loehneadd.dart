import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html_unescape/html_unescape.dart';
import 'package:date_util/date_util.dart';
import 'package:flutter/services.dart';
import 'config.dart';
import 'loehne.dart';

class LoehneAddPage extends StatefulWidget {
  LoehneAddPage() : super();

  @override
  _LoehneAddPageState createState() => _LoehneAddPageState();
}

class _LoehneAddPageState extends State<LoehneAddPage> {
  DateTime _selectedDate = DateTime.now();
  double _stunden = 0;
  String _bezeichnung = "";
  List<DropdownMenuItem<String>> _items = [];

  @override
  void initState() {
    _stunden = 0;
    _bezeichnung = "Putzen";
    _selectedDate = DateTime(Config.selectedYear, Config.selectedMonth, 1);
    getDropDownMenuItems();
    super.initState();
  }

  void addEntry(LohnEntry e) async {
    var body = jsonEncode(e.toJson());
    print("Body: " + body);
    final response = await http.post(Config.baseUrl + "loehne_insert",
        headers: Config.httpHeaders(), body: body);
    print(response.statusCode.toString());
    print(response.body.toString());
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoehnePage()));
  }

  void abort() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoehnePage()));
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("de", "DE"),
      initialDate: _selectedDate,
      firstDate: DateTime(Config.selectedYear, Config.selectedMonth, 1),
      lastDate: DateTime(Config.selectedYear, Config.selectedMonth,
          DateUtil().daysInMonth(Config.selectedMonth, Config.selectedYear)),
    );
    if (picked != null && picked != _selectedDate)
      setState(() {
        _selectedDate = picked;
      });
  }

  Widget form(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child: Container(
            width: 500,
            height: 450,
            padding: EdgeInsets.all(50),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.lightGreen,
            ),
            child: Column(children: [
              Row(
                children: [
                  const Icon(
                    Icons.calendar_today,
                    color: Colors.yellow,
                  ),
                  Container(
                    width: 15,
                  ),
                  ElevatedButton(
                    onPressed: () => _selectDate(context),
                    child: Text(
                      "${_selectedDate.year}-${_selectedDate.month}-${_selectedDate.day}",
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                ],
              ),
              Container(height: 20),
              Container(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  decoration: BoxDecoration(
                    color: Colors.yellow,
                  ),
                  child: DropdownButton(
                    isExpanded: true,
                    value: _bezeichnung,
                    items: _items,
                    style: TextStyle(fontSize: 22, color: Colors.black),
                    onChanged: (s) {
                      setState(() {
                        _bezeichnung = s;
                      });
                    },
                  )),
              Container(height: 20),
              TextFormField(
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9\.]'))
                ],
                onChanged: (s) {
                  _stunden = double.parse(s);
                },
                decoration: InputDecoration(
                  filled: true,
                  icon: const Icon(
                    Icons.schedule,
                    color: Colors.yellow,
                  ),
                  fillColor: Colors.yellow,
                  hintText: "Stunden",
                ),
              ),
              Container(height: 50),
              Row(children: [
                Spacer(),
                ElevatedButton(
                  onPressed: () => addEntry(LohnEntry(
                    id: null,
                    maId: Config.currentMaId,
                    stunden: _stunden,
                    bezeichnung: _bezeichnung,
                    datum:
                        "${_selectedDate.year}-${_selectedDate.month}-${_selectedDate.day}",
                  )),
                  child: Text(
                    "OK",
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                Container(width: 10),
                ElevatedButton(
                  onPressed: () => abort(),
                  child: Text(
                    "Abbruch",
                    style: TextStyle(fontSize: 22),
                  ),
                ),
              ]),
            ])));
  }

  void getDropDownMenuItems() {
    _items.add(DropdownMenuItem(value: "Putzen", child: new Text("Putzen")));
    _items.add(DropdownMenuItem(value: "Küche", child: new Text("Küche")));
    _items.add(
        DropdownMenuItem(value: "Bedienung", child: new Text("Bedienung")));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('Eintrag hinzufügen'),
      ),
      body: form(context),
    );
  }
}
