import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html_unescape/html_unescape.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_mailer/flutter_mailer.dart';
import 'config.dart';
import 'kassenbuch.dart';
import 'loehneadd.dart';

class LohnEntry {
  int id;
  int maId;
  String datum;
  String bezeichnung;
  double stunden;

  LohnEntry({this.id, this.maId, this.datum, this.bezeichnung, this.stunden});

  factory LohnEntry.fromJson(Map<String, dynamic> json) {
    var unescape = new HtmlUnescape();
    return LohnEntry(
      id: int.parse(json['id'] as String),
      maId: int.parse(json['mitarbeiter'] as String),
      datum: json['datum'] as String,
      bezeichnung: unescape.convert(json['bezeichnung'] as String),
      stunden: double.parse(json['stunden'] as String),
    );
  }

  Map<String, dynamic> toJson() {
    var escape = new HtmlEscape();
    Map<String, dynamic> retVal = Map<String, dynamic>();
    retVal['id'] = id.toString();
    retVal['mitarbeiter'] = maId.toString();
    retVal['datum'] = datum.toString();
    retVal['stunden'] = stunden.toString();
    retVal['bezeichnung'] = escape.convert(bezeichnung);
    return retVal;
  }
}

class AngestellterEntry {
  int id;
  String name;

  AngestellterEntry({this.id, this.name});

  factory AngestellterEntry.fromJson(Map<String, dynamic> json) {
    var unescape = new HtmlUnescape();
    return AngestellterEntry(
      id: int.parse(json['id'] as String),
      name: unescape.convert(json['name'] as String),
    );
  }
}

class LoehnePage extends StatefulWidget {
  LoehnePage() : super();

  @override
  _LoehnePageState createState() => _LoehnePageState();
}

class _LoehnePageState extends State<LoehnePage> {
  List<LohnEntry> _entries = [];
  List<AngestellterEntry> _angestellte = [];
  double _stunden = 0;
  int _maIdx = -1;

  @override
  void initState() {
    _angestellte = [];
    _entries = [];
    fetchAngestellte();
    super.initState();
  }

  void fetchAngestellte() async {
    try {
      final response = await http.get(Config.baseUrl + "angestellte_get",
          headers: Config.httpHeaders());
      //print('getEmployees Response: ${response.body}');
      if (200 == response.statusCode) {
        setState(() {
          final parsed =
              json.decode(response.body).cast<Map<String, dynamic>>();
          _angestellte = parsed
              .map<AngestellterEntry>(
                  (json) => AngestellterEntry.fromJson(json))
              .toList();
          _maIdx = 0;
          Config.currentMaId = _angestellte[_maIdx].id;
        });
        fetchEntries();
      } else {
        setState(() {
          _entries = [];
        });
      }
    } catch (e) {
      setState(() {
        _entries = [];
      });
    }
  }

  void fetchEntries() async {
    try {
      final response = await http.get(
          Config.baseUrl +
              "loehne_get/${Config.selectedYear}/${Config.selectedMonth}/${Config.currentMaId}",
          headers: Config.httpHeaders());
      print('getEmployees Response: ${response.body}');
      if (200 == response.statusCode) {
        setState(() {
          _entries = parseResponse(response.body);
        });
      } else {
        setState(() {
          _entries = [];
        });
      }
    } catch (e) {
      setState(() {
        _entries = [];
      });
    }
  }

  void delEntry(LohnEntry e) async {
    final response = await http.post(
        Config.baseUrl + "loehne_delete/" + e.id.toString(),
        headers: Config.httpHeaders());
    print(response.statusCode.toString());
    fetchEntries();
  }

  static List<LohnEntry> parseResponse(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<LohnEntry>((json) => LohnEntry.fromJson(json)).toList();
  }

  List<Widget> getListEntries() {
    List<Widget> retVal = [];
    Config.hasUebertrag = false;
    _stunden = 0;

    for (int i = 0; i < _entries.length; i++) {
      LohnEntry e = _entries.elementAt(i);
      _stunden += e.stunden;

      retVal.add(Card(
        color: i % 2 == 0 ? Colors.lightGreen[50] : Colors.yellow[50],
        child: ListTile(
          leading: Icon(
            Icons.schedule,
            color: Colors.green,
            size: 48,
          ),
          title: Text(e.datum + "\n" + e.bezeichnung),
          subtitle: Text(
            e.stunden.toStringAsFixed(2) + " Std.",
            textAlign: TextAlign.right,
          ),
          trailing: InkWell(
              child: Icon(Icons.delete, size: 28),
              onTap: () {
                delEntry(e);
              }),
          isThreeLine: true,
        ),
      ));
    }

    return retVal;
  }

  Widget navigation() {
    String formattedDate =
        "${Config.selectedYear} - ${Config.monthName(Config.selectedMonth)}";
    return Column(children: [
      Container(
          color: Colors.lightGreen,
          child: Row(children: [
            IconButton(
              onPressed: () {
                Config.decreaseMonth();
                fetchEntries();
              },
              icon: Icon(Icons.arrow_left),
              iconSize: 72,
            ),
            Spacer(),
            Text(formattedDate, style: TextStyle(fontSize: 20)),
            Spacer(),
            IconButton(
              onPressed: () {
                Config.increaseMonth();
                fetchEntries();
              },
              icon: Icon(Icons.arrow_right),
              iconSize: 72,
            ),
          ])),
      Container(
          color: Colors.green,
          child: Row(children: [
            IconButton(
              onPressed: () {
                setState(() {
                  if (_maIdx > 0) {
                    _maIdx--;
                    Config.currentMaId = _angestellte[_maIdx].id;
                  }
                  fetchEntries();
                });
              },
              icon: Icon(Icons.arrow_left),
              iconSize: 72,
            ),
            Spacer(),
            Text(_maIdx >= 0 ? _angestellte[_maIdx].name : "",
                style: TextStyle(fontSize: 20)),
            Spacer(),
            IconButton(
              onPressed: () {
                setState(() {
                  if (_maIdx < _angestellte.length - 1) {
                    _maIdx++;
                    Config.currentMaId = _angestellte[_maIdx].id;
                  }
                  fetchEntries();
                });
              },
              icon: Icon(Icons.arrow_right),
              iconSize: 72,
            ),
          ]))
    ]);
  }

  void _sendViaEmail() async {
    try {
      final response = await http.get(
          Config.baseUrlNoApi +
              "loehne/generatePdf/${Config.selectedYear}/${Config.selectedMonth}",
          headers: Config.httpHeaders());
      if (200 == response.statusCode) {
        Directory tempDir = await getTemporaryDirectory();
        String tempPath = tempDir.path;
        var file = File(
            "$tempPath/Loehne-${Config.selectedYear}-${Config.selectedMonth}.pdf");
        await file.writeAsBytes(response.bodyBytes);

        final MailOptions mailOptions = MailOptions(
          body:
              'Hallo Frau Zahn,\n\nhier die Löhne für ${Config.monthName(Config.selectedMonth)} ${Config.selectedYear}\n\nViele Grüße\nUrsula Scholz',
          subject:
              'Löhne ${Config.selectedYear}-${Config.monthName(Config.selectedMonth)}',
          recipients: ['sandy.zahn@fas.ag'],
          isHTML: false,
          attachments: [
            file.path,
          ],
        );

        final MailerResponse response1 = await FlutterMailer.send(mailOptions);
      } else {
        setState(() {});
      }
    } catch (e) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Löhne'),
        backgroundColor: Colors.green,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.swap_horizontal_circle),
            tooltip: 'Kassenbuch',
            onPressed: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => KassenbuchPage()),
              );
            },
          ),
        ],
      ),
      body: Column(children: [
        navigation(),
        Expanded(
          child: ListView(
            children: getListEntries(),
          ),
        ),
      ]),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => LoehneAddPage()),
          );
        },
        tooltip: 'Neuer Eintrag',
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.lightGreen,
        child: Container(
          height: 50.0,
          child: Row(children: [
            Spacer(),
            Text("Stunden: " + _stunden.toStringAsFixed(2),
                style: TextStyle(fontSize: 20)),
            Spacer(),
            IconButton(icon: Icon(Icons.email), onPressed: _sendViaEmail),
          ]),
        ),
      ),
    );
  }
}
