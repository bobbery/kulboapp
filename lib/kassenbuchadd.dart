import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html_unescape/html_unescape.dart';
import 'package:date_util/date_util.dart';
import 'package:flutter/services.dart';
import 'config.dart';
import 'kassenbuch.dart';

class KassenbuchAddPage extends StatefulWidget {
  KassenbuchAddPage() : super();

  @override
  _KassenbuchAddPageState createState() => _KassenbuchAddPageState();
}

class _KassenbuchAddPageState extends State<KassenbuchAddPage> {
  DateTime _selectedDate = DateTime.now();
  double _betrag = 0;
  var _isSelected = <bool>[true, false];
  bool _einnahme = true;
  String _bezeichnung = "";

  @override
  void initState() {
    _einnahme = true;
    _betrag = 0;
    _bezeichnung = "";
    _selectedDate = DateTime(Config.selectedYear, Config.selectedMonth, 1);
    super.initState();
  }

  void addEntry(Entry e) async {
    var body = jsonEncode(e.toJson());
    //print("Body: " + body);
    final response = await http.post(Config.baseUrl + "kassenbuch_insert",
        headers: Config.httpHeaders(), body: body);
    print(response.statusCode.toString());
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => KassenbuchPage()));
  }

  void abort() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => KassenbuchPage()));
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      locale: const Locale("de", "DE"),
      initialDate: _selectedDate,
      firstDate: DateTime(Config.selectedYear, Config.selectedMonth, 1),
      lastDate: DateTime(Config.selectedYear, Config.selectedMonth,
          DateUtil().daysInMonth(Config.selectedMonth, Config.selectedYear)),
    );
    if (picked != null && picked != _selectedDate)
      setState(() {
        _selectedDate = picked;
      });
  }

  Widget normal(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child: Container(
            width: 500,
            height: 500,
            padding: EdgeInsets.all(50),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.lightBlue,
            ),
            child: Column(children: [
              Container(
                  color: Colors.yellow,
                  child: Row(
                    children: [
                      ToggleButtons(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_upward,
                            color: Colors.green,
                          ),
                          Icon(
                            Icons.arrow_downward,
                            color: Colors.red,
                          ),
                        ],
                        selectedColor: Colors.green,
                        color: Colors.yellow,
                        onPressed: (int index) {
                          setState(() {
                            if (index == 0) {
                              _einnahme = true;
                              _isSelected[1] = false;
                              _isSelected[0] = true;
                            } else {
                              _einnahme = false;
                              _isSelected[0] = false;
                              _isSelected[1] = true;
                            }
                          });
                        },
                        isSelected: _isSelected,
                      ),
                      Container(width: 10),
                      Text(_einnahme ? 'Einnahme' : 'Ausgabe',
                          style: TextStyle(fontSize: 24)),
                    ],
                  )),
              Container(height: 20),
              Row(
                children: [
                  const Icon(
                    Icons.calendar_today,
                    color: Colors.yellow,
                  ),
                  Container(
                    width: 15,
                  ),
                  RaisedButton(
                    onPressed: () => _selectDate(context),
                    child: Text(
                      "${_selectedDate.year}-${_selectedDate.month}-${_selectedDate.day}",
                      style: TextStyle(fontSize: 22),
                    ),
                    color: Colors.yellow,
                  ),
                ],
              ),
              Container(height: 20),
              TextFormField(
                onChanged: (s) {
                  _bezeichnung = s;
                },
                decoration: InputDecoration(
                  filled: true,
                  icon: const Icon(
                    Icons.note,
                    color: Colors.yellow,
                  ),
                  fillColor: Colors.yellow,
                  hintText: "Bezeichnung",
                ),
              ),
              Container(height: 20),
              TextFormField(
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9\.]'))
                ],
                onChanged: (s) {
                  _betrag = double.parse(s);
                },
                decoration: InputDecoration(
                  filled: true,
                  icon: const Icon(
                    Icons.monetization_on,
                    color: Colors.yellow,
                  ),
                  fillColor: Colors.yellow,
                  hintText: "Betrag",
                ),
              ),
              Container(height: 50),
              Row(children: [
                Spacer(),
                ElevatedButton(
                  onPressed: () => addEntry(Entry(
                      id: null,
                      ausgaben: _einnahme == false ? _betrag : 0.0,
                      einnahmen: _einnahme == true ? _betrag : 0.0,
                      bezeichnung: _bezeichnung,
                      datum:
                          "${_selectedDate.year}-${_selectedDate.month}-${_selectedDate.day}",
                      uebertrag: false)),
                  child: Text(
                    "OK",
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                Container(width: 10),
                ElevatedButton(
                  onPressed: () => abort(),
                  child: Text(
                    "Abbruch",
                    style: TextStyle(fontSize: 22),
                  ),
                ),
              ]),
            ])));
  }

  Widget uebertrag() {
    return Align(
        alignment: Alignment.center,
        child: Container(
            width: 500,
            height: 500,
            padding: EdgeInsets.all(50),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.lightBlue,
            ),
            child: Column(children: [
              Text('Noch kein Übertrag gesetzt !!!',
                  style: TextStyle(fontSize: 24)),
              Container(height: 20),
              TextFormField(
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9\.]'))
                ],
                onChanged: (s) {
                  _betrag = double.parse(s);
                },
                decoration: InputDecoration(
                  filled: true,
                  icon: const Icon(
                    Icons.monetization_on,
                    color: Colors.yellow,
                  ),
                  fillColor: Colors.yellow,
                  hintText: "Übertrag",
                ),
              ),
              Container(height: 50),
              Row(children: [
                Spacer(),
                ElevatedButton(
                  onPressed: () => addEntry(Entry(
                      id: null,
                      ausgaben: 0,
                      einnahmen: _betrag,
                      bezeichnung: "Übertrag Kassenbestand Vormonat",
                      datum: '${Config.selectedYear}-${Config.selectedMonth}-1',
                      uebertrag: true)),
                  child: Text(
                    "OK",
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                Container(width: 10),
                ElevatedButton(
                  onPressed: () => abort(),
                  child: Text(
                    "Abbruch",
                    style: TextStyle(fontSize: 22),
                  ),
                ),
              ]),
            ])));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Eintrag hinzufügen'),
      ),
      body: Config.hasUebertrag ? normal(context) : uebertrag(),
    );
  }
}
