import 'dart:convert';

class Config {
  static String baseUrl = 'http://kulbo.roehrach.de/index.php/api/';
  static String baseUrlNoApi = 'http://kulbo.roehrach.de/index.php/';

  static Map<String, String> httpHeaders() {
    final String basicAuth =
        'Basic ' + base64Encode(utf8.encode('uschi:091351704'));

    Map<String, String> headers = {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': basicAuth
    };

    return headers;
  }

  static String monthName(int month) {
    final List<String> _monthNames = [
      "",
      "JANUAR",
      "FEBRUAR",
      "MÄRZ",
      "APRIL",
      "MAI",
      "JUNI",
      "JULI",
      "AUGUST",
      "SEPTEMBER",
      "OKTOBER",
      "NOVEMBER",
      "DEZEMBER"
    ];
    return _monthNames[month];
  }

  static int selectedYear = DateTime.now().year;
  static int selectedMonth = DateTime.now().month;

  static void increaseMonth() {
    selectedMonth += 1;
    if (selectedMonth > 12) {
      selectedYear += 1;
      selectedMonth = 1;
    }
  }

  static void decreaseMonth() {
    selectedMonth -= 1;
    if (selectedMonth == 0) {
      selectedYear -= 1;
      selectedMonth = 12;
    }
  }

  static bool hasUebertrag = false;

  static int currentMaId = 0;
}
