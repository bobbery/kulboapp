import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html_unescape/html_unescape.dart';
import 'config.dart';
import 'kassenbuchadd.dart';
import 'loehne.dart';

class Entry {
  int id;
  String datum;
  bool uebertrag;
  String bezeichnung;
  double einnahmen;
  double ausgaben;

  Entry(
      {this.id,
      this.datum,
      this.uebertrag,
      this.bezeichnung,
      this.einnahmen,
      this.ausgaben});

  factory Entry.fromJson(Map<String, dynamic> json) {
    var unescape = new HtmlUnescape();
    return Entry(
      id: int.parse(json['id'] as String),
      datum: json['datum'] as String,
      uebertrag: (json['uebertrag'] as String) == "1" ? true : false,
      bezeichnung: unescape.convert(json['bezeichnung'] as String),
      einnahmen: double.parse(json['einnahmen'] as String),
      ausgaben: double.parse(json['ausgaben'] as String),
    );
  }

  Map<String, dynamic> toJson() {
    var escape = new HtmlEscape();
    Map<String, dynamic> retVal = Map<String, dynamic>();
    retVal['id'] = id.toString();
    retVal['datum'] = datum.toString();
    retVal['uebertrag'] = uebertrag ? '1' : 'null';
    retVal['bezeichnung'] = escape.convert(bezeichnung);
    retVal['einnahmen'] = einnahmen.toString();
    retVal['ausgaben'] = ausgaben.toString();
    return retVal;
  }
}

class KassenbuchPage extends StatefulWidget {
  KassenbuchPage() : super();

  @override
  _KassenbuchPageState createState() => _KassenbuchPageState();
}

class _KassenbuchPageState extends State<KassenbuchPage> {
  List<Entry> _entries = [];
  double _monatsende = 0;

  @override
  void initState() {
    _entries = [];
    fetchEntries();
    super.initState();
  }

  void fetchEntries() async {
    try {
      final response = await http.get(
          Config.baseUrl +
              "kassenbuch_get/${Config.selectedYear}/${Config.selectedMonth}",
          headers: Config.httpHeaders());
      //print('getEmployees Response: ${response.body}');
      if (200 == response.statusCode) {
        setState(() {
          _entries = parseResponse(response.body);
        });
      } else {
        setState(() {
          _entries = [];
        });
      }
    } catch (e) {
      setState(() {
        _entries = [];
      });
    }
  }

  void delEntry(Entry e) async {
    final response = await http.post(
        Config.baseUrl + "kassenbuch_delete/" + e.id.toString(),
        headers: Config.httpHeaders());
    print(response.statusCode.toString());
    fetchEntries();
  }

  static List<Entry> parseResponse(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Entry>((json) => Entry.fromJson(json)).toList();
  }

  List<Widget> getListEntries() {
    List<Widget> retVal = [];
    Config.hasUebertrag = false;
    _monatsende = 0;

    for (int i = 0; i < _entries.length; i++) {
      Entry e = _entries.elementAt(i);
      _monatsende += e.einnahmen;
      _monatsende -= e.ausgaben;

      if (e.uebertrag == true) {
        Config.hasUebertrag = true;
      }

      retVal.add(Card(
        color: e.uebertrag == true
            ? Colors.yellow
            : i % 2 == 0
                ? Colors.lightBlue[50]
                : Colors.yellow[50],
        child: ListTile(
          leading: e.uebertrag == true
              ? Icon(
                  Icons.monetization_on,
                  color: Colors.green,
                  size: 48,
                )
              : e.ausgaben > 0
                  ? Icon(
                      Icons.arrow_downward,
                      color: Colors.red,
                      size: 48,
                    )
                  : Icon(
                      Icons.arrow_upward,
                      color: Colors.green,
                      size: 48,
                    ),
          title: Text(e.datum + "\n" + e.bezeichnung),
          subtitle: Text(
            e.ausgaben > 0
                ? e.ausgaben.toStringAsFixed(2) + " €"
                : e.einnahmen.toStringAsFixed(2) + " €",
            textAlign: TextAlign.right,
          ),
          trailing: InkWell(
              child: Icon(Icons.delete, size: 28),
              onTap: () {
                delEntry(e);
              }),
          isThreeLine: true,
        ),
      ));
    }

    return retVal;
  }

  Widget navigation() {
    String formattedDate =
        "${Config.selectedYear} - ${Config.monthName(Config.selectedMonth)}";
    return Container(
        color: Colors.lightBlue,
        child: Row(children: [
          IconButton(
            onPressed: () {
              Config.decreaseMonth();
              fetchEntries();
            },
            icon: Icon(Icons.arrow_left),
            iconSize: 72,
          ),
          Spacer(),
          Text(formattedDate, style: TextStyle(fontSize: 20)),
          Spacer(),
          IconButton(
            onPressed: () {
              Config.increaseMonth();
              fetchEntries();
            },
            icon: Icon(Icons.arrow_right),
            iconSize: 72,
          ),
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Kassenbuch'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.swap_horizontal_circle),
            tooltip: 'Löhne',
            onPressed: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => LoehnePage()),
              );
            },
          ),
        ],
      ),
      body: Column(children: [
        navigation(),
        Expanded(
          child: ListView(
            children: getListEntries(),
          ),
        ),
      ]),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => KassenbuchAddPage()),
          );
        },
        tooltip: 'Neuer Eintrag',
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.lightBlue,
        child: Container(
          height: 50.0,
          child: Row(children: [
            Spacer(),
            Text("Monatsende: " + _monatsende.toStringAsFixed(2) + " €",
                style: TextStyle(fontSize: 20)),
            Spacer(),
          ]),
        ),
      ),
    );
  }
}
