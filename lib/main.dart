import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_file.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'kassenbuch.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KuLbo-App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('de', ''), // English, no country code
      ],
    );
  }
}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: KassenbuchPage(),
      title: Text(
        'KuLbo-App',
        textScaleFactor: 4,
      ),
      image: Image(image: AssetImage('data/mama.png')),
      loadingText: Text("Lade ..."),
      photoSize: 150.0,
      loaderColor: Colors.black,
      backgroundColor: Colors.lightBlue,
    );
  }
}
